"name": "vaprzonm2/adroll123123124_pixel",
  "description": "Adroll Pixel for Magento 2.x",
  "type": "magento2-module",
  "version": "1.x"
  
  
Attract
Tap into our pool of 1.2 billion shoppers to find the right audience to grow your site traffic.


Convert
Generate over 5x return on your marketing dollars by retargeting people who already know about you.


Attribute
Join the 37,000+ brands that trust AdRoll to measure the ROI of your marketing spend.